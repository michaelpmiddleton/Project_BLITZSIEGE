﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarScript : MonoBehaviour {
	[SerializeField]
	private Vector3 _healthBarDefaultPosition;
	[SerializeField]
	private UnitStructure _parent;
	[SerializeField]
	private Transform _damageSlider;
	[SerializeField]
	private float _healthPointToTransformRatio;
	[SerializeField]
	// private SpriteRenderer _spriteRenderer;

	public void Initialize () {
		_parent = GetComponentInParent<UnitStructure> ();
		// _spriteRenderer = GetComponent<SpriteRenderer> ();
		_healthPointToTransformRatio = transform.localPosition.x / _parent.MaxHealth;
		transform.localPosition = _healthBarDefaultPosition;
		name = "Healthbar";
	}

	public void Hit (int damage) {
		_damageSlider.localPosition = new Vector3 (
			_damageSlider.localPosition.x - Mathf.Abs(_healthPointToTransformRatio * damage),
			_damageSlider.localPosition.y
		);
	}


	public void UpdateOrderInSortingLayer (int newOrder) {
		// _spriteRenderer.sortingOrder = newOrder;
	}
}
