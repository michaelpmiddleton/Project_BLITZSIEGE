/*
 *	FILE:	UIManagerScript.cs
 *	AUTHOR:	mmiddleton
 *	DATE:	16 SEP 2017
 * 
 * 	DESCRIPTION:
 * 	This script is to be attached to the UI MANAGER Empty. It will hold the code for altering the UI.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagerScript : MonoBehaviour {
    // Public Variables:
    public Text TimerText;              // Text that contains the rounded value of the timer.
    public Text FakeTerminal;           // Text that provides in-game assistance (key mappings, etc.) 
    public bool TitleCardIsShowing;     // True if the _titleCard GameObject is showing.
    public Color FocusColor;            // Color of a tile currently being hovered over.
    public Color HighlightColor;        // Color a tile will change to when the Highlight function is called.
    public float TileWidthHeightRatio;
    public bool GameOverTitleCardLock;   // Boolean that will lock the title screen to prevent the title card timer from changing he screen.
 

    // Private - Manage these in Engine:
    [SerializeField]
    private GameObject _scoreboard;     // UIE containing the timer, player names, and scores for ea. player.
    [SerializeField]
    private GameObject _phase1;         // UIE containing buttons for Phase 1.
    [SerializeField]
    private GameObject _phase2;         // UIE containing buttons for Phase 2.
    [SerializeField]
    private float _timerDefaultValue = 10.0f;
    [SerializeField]
    private Sprite _emptyTileSprite;            // Sprite for the empty tiles.
    [SerializeField]
    private Sprite _plateauTileSprite;          // Sprite for the plateau tiles.
    [SerializeField]
    private Sprite[] _conduitSprites;           // Sprite array containing all Faction Conduit tiles.
    [SerializeField]
    private Sprite[] _attackUnitSprites;        // Sprite array containing all Faction AU's.
    [SerializeField]
    private Sprite[] _defenseUnitSprites;       // Sprite array containing all Faction DU's.
    [SerializeField]
    private Sprite[] _towerSprites;             // Sprite array containing all Faction towers.
    [SerializeField]
    private Sprite[] _trapSprites;              // Sprite array containing all Faction traps.
    [SerializeField]
    private Sprite[] _wallSprites;              // Sprite array containing all Faction walls.
    [SerializeField]
    private Sprite _swarmSprite;                // Sprite for the swarm unit.
    [SerializeField]
    private GameObject _titleCard;              // GameObject containing the titlecard that pauses the flow of the game.
    [SerializeField]
    private Text _titleCardPhase;               // Text element on _titleCard that shows the current phase to the player.
    [SerializeField]
    private Text _titleCardPlayerName;          // Text element on the _titleCard that shows the current player's name.
    [SerializeField]
    private Text _titleCardInstructions;        // Text element on the _titleCard that shows the instructions to the current player.



    // Private Variables:
	private Vector2[] _leftAnchor;		// Anchor position for UI elements when Player1 == Current Player
	private Vector2[] _rightAnchor;		// Anchor position for UI elements when Player2 == CurrentPlayer
    private string[] _terminalPresets = {
        "blitzsiege$  cat Structs.bz\n\nclass Input (Stage_1) {\n\n\tenum UNIT_TYPE {\n\t\tTRAP  = Key.X,\n\t\tTOWER = Key.Y,\n\t\tWALL  = Key.B\n\t};\n\n\tAddCancelCommand (Key.B);\n\n\tAcceptInput();\n}",
        "blitzsiege$  cat Units.sg\n\nclass Input (Stage_2) {\n\n\tenum UNIT_TYPE {\n\t\tTRAP  = Key.X,\n\t\tTOWER = Key.Y,\n\t\tWALL  = Key.B\n\t};\n\n\tAddCancelCommand (Key.B);\n\n\tAcceptInput();\n}",
        "blitzsiege$  ./process_turn\n\n\n> Timer set to 20s.\n> Awaiting player input...\n\nEnd input stream:\t<BACK>",
        "blitzsiege$  ./gameover\nWINNER:\t\tPLAYER ONE\nLOSER:\t\tPLAYER TWO\n\nGood Fight!"
    };
	


	void Start () {
        TimerText.text = ((int)GameManagerScript.Instance.TimerMax).ToString ();
		_UpdateScorebarData (); 		                    // Link names and (TODO) rounds won (/) to the Scoreboard.
        SwitchSides ();                                     // Called to make the title card show proper data.
        TileWidthHeightRatio = _emptyTileSprite.rect.height / _emptyTileSprite.rect.width;
    }




    public void TimerUpdate (float timerValue) {
        TimerText.text = ((int)timerValue + 1).ToString ();
    }



	private void _UpdateScorebarData () {
		_scoreboard.transform.Find ("P1_Name").GetComponentInParent<Text> ().text = GameManagerScript.Instance.PlayerOne.Name;
        _scoreboard.transform.Find ("P1_Score").GetComponentInParent<Text> ().text = GameManagerScript.Instance.PlayerOne.Score.ToString();
        _scoreboard.transform.Find ("P2_Name").GetComponentInParent<Text> ().text = GameManagerScript.Instance.PlayerTwo.Name;
        _scoreboard.transform.Find ("P2_Score").GetComponentInParent<Text> ().text = GameManagerScript.Instance.PlayerTwo.Score.ToString();
	}


    private void _UpdateTerminalText () {
        GameManagerScript.Instance.SoundManager.PlaySound ("Typing");
        TerminalDefault ();
    }



    public void AppendTextToTerminal (string toBeAdded) {
        FakeTerminal.text = _terminalPresets[GameManagerScript.Instance.Phase - 1] + toBeAdded;
    }
    public void TerminalDefault () {
        FakeTerminal.text = _terminalPresets[GameManagerScript.Instance.Phase - 1];
    }



	public void SwitchSides() {
		switch (GameManagerScript.Instance.Phase) {
            case 1:
                _SetPhaseText ("Structure Placement");
                _UpdatePlayerText ();
                _UpdateTerminalText ();
                _SetInstructionsText ("Place ONE of each type of Structure:\n\nX = Trap\nY = Tower\nB = Wall (Select)\t/\tCancel Placement(Structure Selected)\nA = Place Unit\n\nPress \'A\' to Continue");        
                break;

            case 2:
                _SetPhaseText ("Unit Placement");
                _UpdatePlayerText ();
                _UpdateTerminalText ();
                _SetInstructionsText ("Place your units:\n\nX = Attack Unit (If Applicable)\nY = Swarm Unit (If Applicable)\nB = Defense Unit (Select Unit, If Applicable)\t/\tCancel Placement (Unit Selected)\nA = Place Unit\n\nPress \'A\' to Continue");        
                break;

            case 3:
                _SetPhaseText ("Your Turn:");
                _UpdatePlayerText ();
                _UpdateTerminalText ();
                _SetInstructionsText ("X = Activate Trap\nA = Select Unit / Select Movement Location OR Attack Target\n\nPress \'A\' to Continue");        
                break;
        }

        ToggleTitleCardActivation (true); 
    }


    public void ToggleTitleCardActivation (bool value) {
        TitleCardIsShowing = value;
        _titleCard.SetActive (value);
    }



    public Sprite LoadTileSprite (char tileChar) {
        Sprite toBeReturned;

        switch (tileChar) {
            case '-':
                toBeReturned = _emptyTileSprite;
                break;

            case 'X':
                toBeReturned = _plateauTileSprite;
                break;
            
            case 'C':
                toBeReturned = _conduitSprites[(int)GameManagerScript.Instance.PlayerOne.Faction - 1];
                break;

            case 'c':
                toBeReturned = _conduitSprites[(int)GameManagerScript.Instance.PlayerTwo.Faction - 1];
                break;

            default:
                toBeReturned = new Sprite ();
                break;
        }

        return toBeReturned;
    }



	public Sprite LoadUnitStructureSprite (UnitStructure.UnitStructureType unitType) {
		switch (unitType) {
			case UnitStructure.UnitStructureType.TOWER:
				return _towerSprites[(int)GameManagerScript.Instance.CurrentPlayer.Faction - 1];

			case UnitStructure.UnitStructureType.WALL:
				return _wallSprites[(int)GameManagerScript.Instance.CurrentPlayer.Faction - 1];

			case UnitStructure.UnitStructureType.TRAP:
				return _trapSprites[(int)GameManagerScript.Instance.CurrentPlayer.Faction - 1];
			
			case UnitStructure.UnitStructureType.ATTACK:
				return _attackUnitSprites[(int)GameManagerScript.Instance.CurrentPlayer.Faction - 1];

			case UnitStructure.UnitStructureType.DEFENSE:
				return _defenseUnitSprites[(int)GameManagerScript.Instance.CurrentPlayer.Faction - 1];

			case UnitStructure.UnitStructureType.SWARM:
				return _swarmSprite;

			default:
				return new Sprite ();
		}
	}



    public void WinTitleCard (string winnerName) {
        _SetPhaseText ("");
        _SetPlayerText (winnerName + " Wins!");
        _SetInstructionsText ("\n\nPress A to return to the menu.");
        GameOverTitleCardLock = true;
        ToggleTitleCardActivation (true);
    }


 
    private void _SetPhaseText (string phase) {
        _titleCardPhase.text = phase.ToUpper ();
    }
    private void _SetPlayerText (string message) {
        _titleCardPlayerName.text = message;
    }
    private void _UpdatePlayerText () {
        _titleCardPlayerName.text = GameManagerScript.Instance.CurrentPlayer.Name.ToUpper ();
    }
    private void _SetInstructionsText (string instructions) {
        _titleCardInstructions.text = instructions;
    }

    
    // private void _SetAsActive(Vector2 position) {
	// 	GameboardTileScript newActive = _tileContainer.transform.GetChild ((int)(position.x + (position.y * GameboardScript.Height))).GetComponent<GameboardTileScript> ();
	// 	_currentActiveTile.GetComponent <SpriteRenderer> ().color = Color.white;
	// 	newActive.GetComponent <SpriteRenderer> ().color = _activatedColor;
	// 	_currentActiveTile = newActive;
	// }
}