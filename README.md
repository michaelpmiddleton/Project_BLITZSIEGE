# __Project BLITZSIEGE__  
This repository comprises the files for the "Advanced Seminar for Games" (EGP-380P) course I took while studying abroad in Montreal in Fall 2017. [NOTE: This text to later be replaced with the summary of the game.]     
</br>
<div align="center">
    <img alt="Screenshot of Project BLITZSIEGE. (Coming Soon)" src="https://github.com/michaelpmiddleton/Project_BLITZSIEGE/blob/master/screenshot.png" />
</div>
</br>

## _Resources:_  
The following are the links to all of the resources used while working on this project:  
 * [Trello](https://trello.com/b/X671qoOG/adv-seminar-for-game-programming) - Issue Tracking
 * [DevLog](https://devlog.michaelpmiddleton.com/) - My Tumblr Dev Log, hosted via a sub-domain of my website.
</br></br>  

## _Want to Fork?_    
At this time I am requesting that you **do not** fork this code. Thank you for your understanding.  
</br></br>  

## _Got Questions?_ 
I'm happy to answer them! Just [send me an email](mailto:mp.middleton@outlook.com)!